class Solution:
    def romanToInt(self, s):
        """
        :type s: str
        :rtype: int
        """
        pos = 0
        result = 0
        s_length = len(s)
        one = ["M", "C", "X", "I"]
        one_value = [1000, 100, 10, 1]
        five = ["", "D", "L", "V"]
        five_value = [0, 500, 50, 5]
        while pos < s_length:
            cur_letter = s[pos]
            if cur_letter in one:
                index = one.index(cur_letter)
                cur_value = one_value[index]
                result += cur_value
                pos += 1
                if pos == s_length:
                    break
                elif cur_letter != "M" and s[pos] == one[index - 1]:
                    result += cur_value * 8
                    pos += 1
                elif cur_letter != "M" and s[pos] == five[index]:
                    result += cur_value * 3
                    pos += 1
                elif s[pos] == cur_letter:
                    while pos < s_length and s[pos] == "C":
                        result += cur_value
                        pos += 1
            elif cur_letter in five:
                index = five.index(cur_letter)
                cur_value = five_value[index]
                result += cur_value
                pos += 1
                co_one = one[index]
                co_one_value = one_value[index]
                while pos < s_length and s[pos] == co_one:
                    result += co_one_value
                    pos += 1

        return result
        
while True:
    s = input("Enter Roman: ")
    print(Solution().romanToInt(s))