class Solution:
    def isValid(self, s):
        """
        :type s: str
        :rtype: bool
        """
        stack = []
        for l in s:
            if (l == "(") or (l == "[") or (l == "{"):
                stack.append(l)
            elif l == ")":
                if len(stack) == 0:
                    return False
                else:
                    if stack.pop() != "(":
                        return False
            elif l == "]":
                if len(stack) == 0:
                    return False
                else:
                    if stack.pop() != "[":
                        return False
            elif l == "}":
                if len(stack) == 0:
                    return False
                else:
                    if stack.pop() != "{":
                        return False
        return len(stack) == 0

while True:
    parentheses = input("Enter string: ")
    print(Solution().isValid(parentheses))