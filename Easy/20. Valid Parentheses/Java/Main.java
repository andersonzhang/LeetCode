package LeetCode;

import java.util.*;

class Solution {
    public boolean isValid(String s) {
        int length = s.length();
        Stack<Character> stack = new Stack<>();
        for (int i = 0; i < length; i++) {
            char current = s.charAt(i);
            if (current == '(' || current == '[' || current == '{'){
                stack.push(current);
            }else if (current == ')'){
                if (stack.size() != 0 && stack.peek() == '(')
                    stack.pop();
                else
                    return false;
            }else if (current == ']'){
                if (stack.size() != 0 && stack.peek() == '[')
                    stack.pop();
                else
                    return false;
            }else if (current == '}'){
                if (stack.size() != 0 && stack.peek() == '{')
                    stack.pop();
                else
                    return false;
            }else
                return false;
        }
        return stack.size() == 0;
    }
}


public class Main {
    public static void main(String args[]) {
        while (true) {
            Scanner get = new Scanner(System.in);
            System.out.println(new Solution().isValid(get.next()));
        }
    }

    public static void readArray(Scanner get, List<Integer> nums) {
        String digits = get.nextLine();
        digits = digits.substring(1, digits.length() - 1);
        for (String num : digits.split(",")) {
            nums.add(Integer.valueOf(num.trim()));
        }
    }
}

