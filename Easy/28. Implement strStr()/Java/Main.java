import java.util.*;

class ListNode {
    int val;
    ListNode next;

    ListNode(int x) {
        val = x;
    }
}

class Solution {
    public int strStr(String haystack, String needle) {
        if (needle.isEmpty())
            return 0;
        if (haystack.isEmpty())
            return -1;
        int index = 0;
        int haystack_length = haystack.length(), needle_length = needle.length();
        if (needle_length > haystack_length)
            return -1;
        char firstMatch = needle.charAt(0);
        while (index < haystack_length) {
            if (haystack.charAt(index) == firstMatch) {
                int travel = index, travel_end = travel + needle_length;
                if (travel_end > haystack_length)
                    return -1;
                while (travel < travel_end && haystack.charAt(travel) == needle.charAt(travel - index))
                    ++travel;
                if (travel == travel_end)
                    return index;
            }
            ++index;
        }
        return -1;
    }
}


public class Main {
    public static void main(String args[]) {
        while (true) {
            Scanner get = new Scanner(System.in);
            System.out.println(new Solution().strStr(get.nextLine().trim(), get.nextLine().trim()));
        }
    }

    static void readArray(Scanner get, List<Integer> nums) {
        String digits = get.nextLine();
        digits = digits.substring(1, digits.length() - 1);
        if (digits.trim().length() == 0)
            return;
        for (String num : digits.split(",")) {
            nums.add(Integer.valueOf(num.trim()));
        }
    }

    static ListNode arrayToList(Scanner get) {
        List<Integer> list = new ArrayList<>();
        readArray(get, list);
        ListNode head = null, move = null;
        for (Integer i : list) {
            if (head == null) {
                head = new ListNode(i);
                move = head;
            } else {
                move.next = new ListNode(i);
                move = move.next;
            }
        }
        return head;
    }

    static ListNode arrayToList(String input) {
        List<Integer> list = new ArrayList<>();
        input = input.substring(1, input.length() - 1);
        for (String num : input.split(",")) {
            list.add(Integer.valueOf(num.trim()));
        }
        ListNode head = null, move = null;
        for (Integer i : list) {
            if (head == null) {
                head = new ListNode(i);
                move = head;
            } else {
                move.next = new ListNode(i);
                move = move.next;
            }
        }
        return head;
    }

    static ListNode[] arrayToListHeads(Scanner get) {
        String arrays = get.nextLine().trim();
        arrays = arrays.substring(1, arrays.length() - 1);
        String splited[] = arrays.split("],\\[");
        if (splited.length == 1 && splited[0].length() == 0)
            return new ListNode[0];
        ListNode result[] = new ListNode[arrays.split("],\\[").length];
        int i = 0;
        for (String sub : arrays.split("],\\[")) {
            if (sub.isEmpty())
                continue;
            sub = sub.trim();
            if (sub.charAt(0) != '[')
                sub = "[" + sub;
            if (sub.charAt(sub.length() - 1) != ']')
                sub = sub + "]";
            result[i] = arrayToList(sub);
            ++i;
        }
        return result;
    }
}

