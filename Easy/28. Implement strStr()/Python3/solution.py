class Solution:
    def strStr(self, haystack, needle):
        """
        :type haystack: str
        :type needle: str
        :rtype: int
        """
        if len(needle) == 0:
            return 0
        if len(needle) > len(haystack):
            return -1
        i = 0
        haystack_len = len(haystack)
        needle_len = len(needle)
        while i < haystack_len:
            if haystack_len - i < needle_len:
                break
            if haystack[i] == needle[0]:
                j = 1
                k = i + 1
                while j < needle_len and k < haystack_len and haystack[k] == needle[j]:
                    k += 1
                    j += 1
                if j == len(needle):
                    return i
            i += 1
        return -1

haystack = input("Enter haystack: ")
needle = input("Enter needle: ")
print(Solution().strStr(haystack, needle))