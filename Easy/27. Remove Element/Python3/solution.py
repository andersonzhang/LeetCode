class Solution:
    def removeElement(self, nums, val):
        """
        :type nums: List[int]
        :type val: int
        :rtype: int
        """
        length = 0
        move_i = 0
        while move_i < len(nums):
            if nums[move_i] != val:
                if length != move_i:
                    nums[length] = nums[move_i]
                length += 1
                move_i += 1
            else:
                while move_i < len(nums) and nums[move_i] == val:
                    move_i += 1
        return length

while True:
    nums = [int(n.strip()) for n in input("Enter nums: ")[1:-1].split(",")]
    val = int(input("Enter target: "))
    print(Solution().removeElement(nums, val))
    print(nums)