class Solution:
    def longestCommonPrefix(self, strs):
        """
        :type strs: List[str]
        :rtype: str
        """
        if len(strs) == 0:
            return ""
        if len(strs) == 1:
            return strs[0]
        prefix = ""
        pos = 0
        str_length = [len(s) for s in strs]
        max_prefix = min(str_length)
        first_s = strs[0]
        while pos < max_prefix:
            cur_letter = first_s[pos]
            to_check = [s[pos] == cur_letter for s in strs]
            if all(to_check):
                prefix += cur_letter
            else:
                break
            pos += 1
        return prefix

while True:
    epoch = int(input("Number of strings: "))
    strs = list()
    for i in range(epoch):
        s = input("Input element %d : " % (i))
        strs.append(s)
    print(Solution().longestCommonPrefix(strs))