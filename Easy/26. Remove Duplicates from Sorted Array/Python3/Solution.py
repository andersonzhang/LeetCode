class Solution:
    def removeDuplicates(self, nums):
        """
        :type nums: List[int]
        :rtype: int
        """
        unique_index = 0
        move = 0
        while move < len(nums):
            move += 1
            while (move < len(nums)) and (nums[move] == nums[move - 1]):
                move += 1
            nums[unique_index] = nums[move - 1]
            unique_index += 1
        return unique_index
        
while True:
    nums = [int(n.strip()) for n in input("Enter nums: ")[1:-1].split(",")]
    print(Solution().removeDuplicates(nums))
    print(nums)