import java.util.*;

class ListNode {
    int val;
    ListNode next;

    ListNode(int x) {
        val = x;
    }
}

class Solution {
    public ListNode mergeTwoLists(ListNode l1, ListNode l2) {
        if (l1 == null)
            return l2;
        if (l2 == null)
            return l1;
        ListNode head, move;
        if (l1.val > l2.val){
            head = l2;
            l2 = l2.next;
        }else{
            head = l1;
            l1 = l1.next;
        }
        move = head;
        while (l1 != null && l2 != null){
            if (l1.val < l2.val){
                move.next = l1;
                l1 = l1.next;
            }else{
                move.next = l2;
                l2 = l2.next;
            }
            move = move.next;
        }
        if (l1 != null){
            move.next = l1;
        }else{
            move.next = l2;
        }
        return head;
    }
}


public class Main {
    public static void main(String args[]) {
        while (true) {
            Scanner get = new Scanner(System.in);
            ListNode l1 = arrayToList(get), l2 = arrayToList(get);
            System.out.println(new Solution().mergeTwoLists(l1, l2));
        }
    }

    public static void readArray(Scanner get, List<Integer> nums) {
        String digits = get.nextLine();
        digits = digits.substring(1, digits.length() - 1);
        for (String num : digits.split(",")) {
            nums.add(Integer.valueOf(num.trim()));
        }
    }

    static ListNode arrayToList(Scanner get){
        List<Integer> list = new ArrayList<>();
        readArray(get, list);
        ListNode head = null, move = null;
        for(Integer i : list){
            if (head == null){
                head = new ListNode(i);
                move = head;
            }else {
                move.next = new ListNode(i);
                move = move.next;
            }
        }
        return head;
    }
}

