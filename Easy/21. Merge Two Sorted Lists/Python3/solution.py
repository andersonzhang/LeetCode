class ListNode:
    def __init__(self, x):
        self.val = x
        self.next = None


class Solution:
    def mergeTwoLists(self, l1, l2):
        """
        :type l1: ListNode
        :type l2: ListNode
        :rtype: ListNode
        """
        if (l1 == None):
            return l2
        if (l2 == None):
            return l1
        head = None
        if (l1.val < l2.val):
            head = l1
            l1 = l1.next
        else:
            head = l2
            l2 = l2.next
        move = head
        while (l1 != None) and (l2 != None):
            if (l1.val < l2.val):
                move.next = l1
                l1 = l1.next
            else:
                move.next = l2
                l2 = l2.next
            move = move.next
        if l1 != None:
            move.next = l1
        if l2 != None:
            move.next = l2
        return head

while True:            
    list = input("Enter list 1: ")[1:-1]
    list = [int(i.strip()) for i in list.split(",")]
    head1 = None
    move = head1
    for i in list:
        if head1 == None:
            head1 = ListNode(i)
            move = head1
        else:
            move.next = ListNode(i)
            move = move.next
    list = input("Enter list 2: ")[1:-1]
    list = [int(i.strip()) for i in list.split(",")]
    head2 = None
    move = head2
    for i in list:
        if head2 == None:
            head2 = ListNode(i)
            move = head2
        else:
            move.next = ListNode(i)
            move = move.next
    print(Solution().mergeTwoLists(head1, head2))
