import queue

class ListNode:
    def __init__(self, x):
        self.val = x
        self.next = None

class PriorityEntry(object):
    def __init__(self, data):
        self.data = data

    def __lt__(self, other):
        return self.data.val < other.data.val

class Solution:
    def mergeKLists(self, lists):
        """
        :type lists: List[ListNode]
        :rtype: ListNode
        """
        q = queue.PriorityQueue()
        for l in lists:
            if l != None:
                q.put(PriorityEntry(l))
        head = None
        move = head
        while not q.empty():
            entry = q.get()
            node = entry.data
            if head == None:
                head = node
                move = head
            else:
                move.next = node
                move = move.next
            if node.next != None:
                q.put(PriorityEntry(node.next))
        return head


def readArray(string):
    return [int(n.strip()) for n in string[1:-1].split(",")]

def arrayToList(array):
    array = array[1:-1]
    array = [int(i.strip()) for i in array.split(",")]
    head = None
    move = head
    for i in array:
        if head == None:
            head = ListNode(i)
            move = head
        else:
            move.next = ListNode(i)
            move = move.next
    return head

def arrayToListHeads(lists):
    lists = lists[1:-1]
    lists = [i.strip() for i in lists.split("],[")]
    heads = list()
    for i in lists:
        if (i[0] != "["):
            i = "[" + i
        if (i[-1] != "]"):
            i = i + "]"
        heads.append(arrayToList(i))
    return heads

def printList(head):
    while head != None:
        print(head.val)
        head = head.next


lists = input("Enter lists: ")
heads = arrayToListHeads(lists)
printList(Solution().mergeKLists(heads))