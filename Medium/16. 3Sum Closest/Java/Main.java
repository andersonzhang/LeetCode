package LeetCode;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

class Solution {
    public int threeSumClosest(int[] nums, int target) {
        Arrays.sort(nums);
        int i = 0;
        int best = Integer.MIN_VALUE;
        while (i < nums.length) {
            int start = i + 1, end = nums.length - 1;
            while (start < end) {
                int sum = nums[i] + nums[start] + nums[end];
                int origin_gap = best == Integer.MIN_VALUE ? Integer.MAX_VALUE : Math.abs(best - target);
                int current_gap = Math.abs(sum - target);
                if (sum == target) {
                    return target;
                } else if (sum < target) {
                    start++;
                    while (start < end && nums[start] == nums[start - 1])
                        start++;
                } else {
                    end--;
                    while (start < end && nums[end] == nums[end + 1])
                        end--;
                }
                if (current_gap < origin_gap) {
                    best = sum;
                }
            }
            i++;
            while (i < nums.length && nums[i] == nums[i - 1])
                i++;
        }
        return best;
    }
}


public class Main {
    public static void main(String args[]) {
        while (true) {
            Scanner get = new Scanner(System.in);
            String array_in_list = get.nextLine().trim();
            array_in_list = array_in_list.substring(1, array_in_list.length() - 1);
            List<Integer> nums = new ArrayList<>();
            for (String element : array_in_list.split(",")) {
                nums.add(Integer.valueOf(element.trim()));
            }
            System.out.println(new Solution().threeSumClosest(nums.stream().mapToInt(Integer::intValue).toArray(), get.nextInt()));
        }
    }
}
