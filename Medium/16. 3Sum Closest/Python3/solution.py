class Solution:
    def threeSumClosest(self, nums, target):
        """
        :type nums: List[int]
        :type target: int
        :rtype: int
        """
        nums.sort()
        bestSum = 1000000000000
        i = 0
        nums_len = len(nums)
        while i < nums_len:
            start = i + 1
            end = nums_len - 1
            while start < end:
                sum = nums[i] + nums[start] + nums[end]
                if sum == target:
                    return sum
                elif sum < target:
                    start += 1
                    while start < end and nums[start] == nums[start - 1]:
                        start += 1
                elif sum > target:
                    end -= 1
                    while start < end and nums[end] == nums[end + 1]:
                        end -= 1

                if abs(sum - target) < abs(bestSum - target):
                    bestSum = sum
            i += 1
            while start < end and nums[i] == nums[i - 1]:
                i += 1
        return bestSum


while True:
    nums_string = input("Enter nums: ")
    nums = [int(i.strip()) for i in nums_string[1:-1].split(",")]
    target = int(input("Enter target: "))
    print(Solution().threeSumClosest(nums, target))
