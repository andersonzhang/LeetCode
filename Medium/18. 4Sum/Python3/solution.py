class Solution:
    def fourSum(self, nums, target):
        """
        :type nums: List[int]
        :type target: int
        :rtype: List[List[int]]
        """
        result = []
        nums.sort()
        i = 0
        nums_length = len(nums)
        while i < nums_length:
            j = i + 1
            while j < nums_length:
                start = j + 1
                end = nums_length - 1
                while start < end:
                    sum = nums[i] + nums[j] + nums[start] + nums[end]
                    if sum == target:
                        result.append([nums[i], nums[j], nums[start], nums[end]])
                        start += 1
                        while start < end and nums[start] == nums[start - 1]:
                            start += 1
                        end -= 1
                        while start < end and nums[end] == nums[end + 1]:
                            end -= 1
                    elif sum < target:
                        start += 1
                        while start < end and nums[start] == nums[start - 1]:
                            start += 1
                    else:
                        end -= 1
                        while start < end and nums[end] == nums[end + 1]:
                            end -= 1
                j += 1
                while j < nums_length and nums[j] == nums[j - 1]:
                    j += 1
            i += 1
            while i < nums_length and nums[i] == nums[i - 1]:
                i += 1
        return result

        

while True:
    nums = [int(n.strip()) for n in input("Enter nums: ")[1:-1].split(",")]
    target = int(input("Enter target: "))
    print(Solution().fourSum(nums, target))