package LeetCode;

import java.util.*;

class Solution {

    public List<List<Integer>> fourSum(int[] nums, int target) {
        List<List<Integer>> result = new ArrayList<>();
        if (nums.length == 0)
            return result;
        Arrays.sort(nums);
        int length = nums.length;
        int i = 0;
        while (i < length){
            int j = i + 1;
            while (j < length){
                int start = j + 1, end = length - 1;
                while (start < end){
                    int sum = nums[i] + nums[j] + nums[start] + nums[end];
                    if (sum == target){
                        result.add(Arrays.asList(nums[i], nums[j], nums[start], nums[end]));
                        start++;
                        while (start < end && nums[start] == nums[start - 1])
                            start++;
                        end--;
                        while (start < end && nums[end] == nums[end + 1])
                            end--;
                    }else if (sum > target){
                        end--;
                        while (start < end && nums[end] == nums[end + 1])
                            end--;
                    }else{
                        start++;
                        while (start < end && nums[start] == nums[start - 1])
                            start++;
                    }
                }
                j++;
                while (j < length && nums[j] == nums[j - 1])
                    j++;
            }
            i++;
            while (i < length && nums[i] == nums[i - 1])
                i++;
        }
        return result;
    }
}


public class Main {
    public static void main(String args[]) {
        while (true) {
            Scanner get = new Scanner(System.in);
            List<Integer> nums = new ArrayList<>();
            readArray(get, nums);
            new Solution().fourSum(nums.stream().mapToInt(e -> (int) e).toArray(), get.nextInt());
        }
    }

    public static void readArray(Scanner get, List<Integer> nums){
        String digits = get.nextLine();
        digits = digits.substring(1, digits.length() - 1);
        for (String num : digits.split(",")){
            nums.add(Integer.valueOf(num.trim()));
        }
    }
}

