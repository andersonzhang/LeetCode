class Solution:
    def threeSum(self, nums):
        """
        :type nums: List[int]
        :rtype: List[List[int]]
        """
        nums.sort()
        result = list()
        i = 0
        while i < len(nums) - 2:
            start = i + 1
            end = len(nums) - 1
            while start < end:
                sum = nums[i] + nums[start] + nums[end]
                if sum == 0:
                    result.append([nums[i], nums[start], nums[end]])
                    start += 1
                    while start < end and nums[start] == nums[start - 1]:
                        start += 1
                    end -= 1
                    while start < end and nums[end] == nums[end + 1]:
                        end -= 1
                elif sum > 0:
                    end -= 1
                    while start < end and nums[end] == nums[end + 1]:
                        end -= 1
                elif sum < 0:
                    start += 1
                    while start < end and nums[start] == nums[start - 1]:
                        start += 1
            i += 1
            while i < len(nums) and nums[i] == nums[i - 1]:
                i += 1
        return result

while True:
    array_size = int(input("Array size: "))
    nums = list()
    for i in range(array_size):
        num = int(input("Element %d : " % (i)))
        nums.append(num)
    print(Solution().threeSum(nums))
