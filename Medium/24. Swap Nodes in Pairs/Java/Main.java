import java.util.*;


class ListNode {
    int val;
    ListNode next;

    ListNode(int x) {
        val = x;
    }
}

class Solution {
    public ListNode swapPairs(ListNode head) {
        if (head == null || head.next == null){
            return head;
        }
        ListNode move = head;
        ListNode second = head.next;
        head = second;
        move.next = second.next;
        second.next = move;
        move = second.next;
        while (move != null && move.next != null && move.next.next != null){
            second = move.next.next;
            move.next.next = second.next;
            second.next = move.next;
            move.next = second;
            move = move.next.next;
        }
        return head;
    }
}


public class Main {
    public static void main(String args[]) {
        while (true) {
            Scanner get = new Scanner(System.in);
            System.out.println(new Solution().swapPairs(arrayToList(get)));
        }
    }

    static void readArray(Scanner get, List<Integer> nums) {
        String digits = get.nextLine();
        digits = digits.substring(1, digits.length() - 1);
        if (digits.trim().length() == 0)
            return;
        for (String num : digits.split(",")) {
            nums.add(Integer.valueOf(num.trim()));
        }
    }

    static ListNode arrayToList(Scanner get) {
        List<Integer> list = new ArrayList<>();
        readArray(get, list);
        ListNode head = null, move = null;
        for (Integer i : list) {
            if (head == null) {
                head = new ListNode(i);
                move = head;
            } else {
                move.next = new ListNode(i);
                move = move.next;
            }
        }
        return head;
    }

    static ListNode arrayToList(String input) {
        List<Integer> list = new ArrayList<>();
        input = input.substring(1, input.length() - 1);
        for (String num : input.split(",")) {
            list.add(Integer.valueOf(num.trim()));
        }
        ListNode head = null, move = null;
        for (Integer i : list) {
            if (head == null) {
                head = new ListNode(i);
                move = head;
            } else {
                move.next = new ListNode(i);
                move = move.next;
            }
        }
        return head;
    }

    static ListNode[] arrayToListHeads(Scanner get) {
        String arrays = get.nextLine().trim();
        arrays = arrays.substring(1, arrays.length() - 1);
        String splited[] = arrays.split("],\\[");
        if (splited.length == 1 && splited[0].length() == 0)
            return new ListNode[0];
        ListNode result[] = new ListNode[arrays.split("],\\[").length];
        int i = 0;
        for(String sub : arrays.split("],\\[")){
            if (sub.isEmpty())
                continue;
            sub = sub.trim();
            if (sub.charAt(0) != '[')
                sub = "[" + sub;
            if (sub.charAt(sub.length() - 1) != ']')
                sub = sub + "]";
            result[i] = arrayToList(sub);
            ++i;
        }
        return result;
    }
}

