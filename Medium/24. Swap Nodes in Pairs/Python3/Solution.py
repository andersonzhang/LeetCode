
class ListNode:
    def __init__(self, x):
        self.val = x
        self.next = None

class Solution:
    def swapPairs(self, head):
        """
        :type head: ListNode
        :rtype: ListNode
        """
        if (head == None) or (head.next == None):
            return head
        move = head
        second = move.next
        move.next = second.next
        head = second
        head.next = move
        while (move != None) and (move.next != None) and (move.next.next != None):
            second = move.next.next
            move.next.next = second.next
            second.next = move.next
            move.next = second
            move = second.next
        return head
            


list = input("Enter list: ")[1:-1]
list = [int(i.strip()) for i in list.split(",")]
head = None
move = head
for i in list:
    if head == None:
        head = ListNode(i)
        move = head
    else:
        move.next = ListNode(i)
        move = move.next
print(Solution().swapPairs(head))