
class Solution:
    def intToRoman(self, num):
        """
        :type num: int
        :rtype: str
        """
        one = ["M", "C", "X", "I"]
        five = ["", "D", "L", "V"]
        i = 0
        pos = 1000
        result = ""
        while pos != 0:
            cur_digit = int(num / pos)
            if cur_digit == 9:
                result += one[i] + one[i - 1]
            elif cur_digit >= 5:
                result += five[i]
                for _ in range(5, cur_digit):
                    result += one[i]
            elif cur_digit == 4:
                result += one[i] + five[i]
            else:
                for _ in range(0, cur_digit):
                    result += one[i]
            num = num % pos
            pos /= 10
            i += 1
        return result

while True:
    num_ = int(input("Enter string: "))
    print(Solution().intToRoman(num_))
    print("\n\n")