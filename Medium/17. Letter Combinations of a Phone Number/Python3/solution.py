class Solution:
    def letterCombinations(self, digits):
        """
        :type digits: str
        :rtype: List[str]
        """
        dialer = {'2': ["a", "b", "c"],
                  '3': ["d", "e", "f"],
                  '4': ["g", "h", "i"],
                  '5': ["j", "k", "l"],
                  '6': ["m", "n", "o"],
                  '7': ["p", "q", "r", "s"],
                  '8': ["t", "u", "v"],
                  '9': ["w", "x", "y", "z"]}
        result = []
        for i in digits:
            if len(result) == 0:
                result = dialer[i]
            else:
                tmp = []
                cur_ = dialer[i]
                for c in cur_:
                    for r in result:
                        tmp.append(r + c)
                result = tmp
        return result


while True:
    digits = input("Enter digits: ")
    print(Solution().letterCombinations(digits))
