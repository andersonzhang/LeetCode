import java.util.*;

class Solution {

    List<String> result = new ArrayList<>();
    Map<Character, Character[]> board = new HashMap<>();
    {
        board.put('2', new Character[]{'a', 'b', 'c'});
        board.put('3', new Character[]{'d', 'e', 'f'});
        board.put('4', new Character[]{'g', 'h', 'i'});
        board.put('5', new Character[]{'j', 'k', 'l'});
        board.put('6', new Character[]{'m', 'n', 'o'});
        board.put('7', new Character[]{'p', 'q', 'r', 's'});
        board.put('8', new Character[]{'t', 'u', 'v'});
        board.put('9', new Character[]{'w', 'x', 'y', 'z'});
    }

    public List<String> letterCombinations(String digits) {
        if (digits.length() == 0) {
            return result;
        }
        Character first_letter = digits.charAt(0);
        List<String> new_result = new ArrayList<>();
        Character[] digit = board.get(first_letter);
        if (result.size() == 0){
            for (Character d : digit) {
                new_result.add(String.valueOf(d));
            }
        }else{
            for (String r : result) {
                for (Character d : digit) {
                    new_result.add(r + d);
                }
            }
        }
        result = new_result;
        return letterCombinations(digits.substring(1));
    }
}


public class Main {
    public static void main(String args[]) {
        while (true) {
            Scanner get = new Scanner(System.in);
            String digits = get.next();
            new Solution().letterCombinations(digits);
        }
    }
}