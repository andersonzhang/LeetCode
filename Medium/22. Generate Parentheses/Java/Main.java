import java.util.*;


class ListNode {
    int val;
    ListNode next;

    ListNode(int x) {
        val = x;
    }
}

class Solution {
    List<String> result = new ArrayList<>();

    public List<String> generateParenthesis(int n) {
        doGenerate(0, 0, n * 2, "");
        return result;
    }

    private void doGenerate(int left, int right, int total, String pre) {
        if (total == pre.length()) {
            result.add(pre);
            return;
        }
        if (left == right) {
            doGenerate(left + 1, right, total, pre + "(");
        } else if (left == total / 2) {
            doGenerate(left, right + 1, total, pre + ")");
        } else if (left > right) {
            doGenerate(left + 1, right, total, pre + "(");
            doGenerate(left, right + 1, total, pre + ")");
        }
    }
}


public class Main {
    public static void main(String args[]) {
        while (true) {
            Scanner get = new Scanner(System.in);
            System.out.println(new Solution().generateParenthesis(get.nextInt()));
        }
    }

    public static void readArray(Scanner get, List<Integer> nums) {
        String digits = get.nextLine();
        digits = digits.substring(1, digits.length() - 1);
        for (String num : digits.split(",")) {
            nums.add(Integer.valueOf(num.trim()));
        }
    }

    static ListNode arrayToList(Scanner get) {
        List<Integer> list = new ArrayList<>();
        readArray(get, list);
        ListNode head = null, move = null;
        for (Integer i : list) {
            if (head == null) {
                head = new ListNode(i);
                move = head;
            } else {
                move.next = new ListNode(i);
                move = move.next;
            }
        }
        return head;
    }
}

