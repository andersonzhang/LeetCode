class Solution:
    result = list()
    def generateParenthesis(self, n):
        """
        :type n: int
        :rtype: List[str]
        """
        self.result = list()
        self.doGenerate(0, 0, n*2, "")
        return self.result

    def doGenerate(self, left, right, total, pre):
        if (left + right == total):
            self.result.append(pre)
            return
        if (left == total / 2):
            self.doGenerate(left, right + 1, total, pre + ")")
        elif left == right:
            self.doGenerate(left + 1, right, total, pre + "(")
        elif left > right:
            self.doGenerate(left, right + 1, total, pre + ")")
            self.doGenerate(left + 1, right, total, pre + "(")

while True:
    n = int(input("Enter number: "))
    print(Solution().generateParenthesis(n))
