class ListNode:
    def __init__(self, x):
        self.val = x
        self.next = None

class Solution:
    def removeNthFromEnd(self, head, n):
        """
        :type head: ListNode
        :type n: int
        :rtype: ListNode
        """
        i = 0
        faster = head
        while i < n:
            faster = faster.next
            if faster == None:
                return head.next
            i += 1
        move = head
        if faster == None:
            head = head.next
        while faster.next != None:
            move = move.next
            faster = faster.next
        move.next = move.next.next
        return head

list = input("Enter list: ")[1:-1]
list = [int(i.strip()) for i in list.split(",")]
head = None
move = head
for i in list:
    if head == None:
        head = ListNode(i)
        move = head
    else:
        move.next = ListNode(i)
        move = move.next
n = int(input("Enter k: "))
print(Solution().removeNthFromEnd(head, n))
