package LeetCode;

import java.util.*;

class ListNode {
    int val;
    ListNode next;

    ListNode(int x) {
        val = x;
    }
}

class Solution {
    public ListNode removeNthFromEnd(ListNode head, int n) {
        ListNode faster = head;
        for (int i = 0; i < n; i++) {
            if (faster == null)
                return head;
            faster = faster.next;
        }
        if (faster == null){
            head = head.next;
            return head;
        }
        ListNode beforeDelete = head;
        while (faster.next != null){
            faster = faster.next;
            beforeDelete = beforeDelete.next;
        }
        beforeDelete.next = beforeDelete.next.next;
        return head;
    }
}


public class Main {
    public static void main(String args[]) {
        while (true) {
            Scanner get = new Scanner(System.in);
            List<Integer> nums = new ArrayList<>();
            readArray(get, nums);
            ListNode head = null, move = head;
            for (Integer num : nums){
                if (head == null){
                    head = new ListNode(num);
                    move = head;
                }else{
                    move.next = new ListNode(num);
                    move = move.next;
                }
            }
            new Solution().removeNthFromEnd(head, get.nextInt());
        }
    }

    public static void readArray(Scanner get, List<Integer> nums) {
        String digits = get.nextLine();
        digits = digits.substring(1, digits.length() - 1);
        for (String num : digits.split(",")) {
            nums.add(Integer.valueOf(num.trim()));
        }
    }
}

