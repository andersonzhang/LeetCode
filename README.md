|ID|Name|C++|Java|Python|
|-|-|-|-|-|
|1|Two Sum|√|√|√|
|2|Add Two Numbers|√|√|√|
|3|Longest Substring Without Repeating Characters|√|√|√|
|4|Median of Two Sorted Arrays||||
|5|Longest Palindromic Substring|√|√|√|
|6|ZigZag Conversion|√|√|√|
|7|Reverse Integer|√|√|√|
|8|String to Integer (atoi)|√|√|√|
|9|Palindrome Number|√|√|√|
|10|Regular Expression Matching||||
|11|Container With Most Water|√|√||
|12|Integer to Roman|√|√|√|
|13|Roman to Integer|√|√|√|
|14|Longest Common Prefix|√|√|√|
|15|3Sum|√|√|√|
|16|3Sum Closest|√|√|√|
|17|Letter Combinations of a Phone Number|√|√|√|
|18|4Sum|√|√|√|
|19|Remove Nth Node From End of List|√|√|√|
|20|Valid Parentheses|√|√|√|
|21|Merge Two Sorted Lists||√|√|
|22|Generate Parentheses||√|√|
|23|Merge k Sorted Lists||√|√|
|24|Swap Nodes in Pairs||√|√|
|25|Reverse Nodes in k-Group||||
|26|Remove Duplicates from Sorted Array||√|√|
|27|Remove Element||√|√|
|28|Implement strStr()||√|√|
|29|Divide Two Integers||||
|30|Substring with Concatenation of All Words||||
|31|Next Permutation||||
|32|Longest Valid Parentheses||||
|33|Search in Rotated Sorted Array||||
|34|Find First and Last Position of Element in Sorted Array||||
|35|Search Insert Position||||
|36|Valid Sudoku||||
|37|Sudoku Solver||||
|38|Count and Say||||
|39|Combination Sum||||
|40|Combination Sum II||||
|41|First Missing Positive||||
|42|Trapping Rain Water||||
|43|Multiply Strings||||
|44|Wildcard Matching||||
|45|Jump Game II||||
|46|Permutations||||
|47|Permutations II||||
|48|Rotate Image||||
|49|Group Anagrams||||
|50|Pow(x, n)||||
|51|N-Queens||||
|52|N-Queens II||||
|53|Maximum Subarray||||
|54|Spiral Matrix||||
|55|Jump Game||||
|56|Merge Intervals||||
|57|Insert Interval||||
|58|Length of Last Word||||
|59|Spiral Matrix II||||
|60|Permutation Sequence||||
|61|Rotate List||||
|62|Unique Paths||||
|63|Unique Paths II||||
|64|Minimum Path Sum||||
|65|Valid Number||||
|66|Plus One||||
|67|Add Binary||||
|68|Text Justification||||
|69|Sqrt(x)||||
|70|Climbing Stairs||||
|71|Simplify Path||||
|72|Edit Distance||||
|73|Set Matrix Zeroes||||
|74|Search a 2D Matrix||||
|75|Sort Colors||||
|76|Minimum Window Substring||||
|77|Combinations||||
|78|Subsets||||
|79|Word Search||||
|80|Remove Duplicates from Sorted Array II||||
|81|Search in Rotated Sorted Array II||||
|82|Remove Duplicates from Sorted List II||||
|83|Remove Duplicates from Sorted List||||
|84|Largest Rectangle in Histogram||||
|85|Maximal Rectangle||||
|86|Partition List||||
|87|Scramble String||||
|88|Merge Sorted Array||||
|89|Gray Code||||
|90|Subsets II||||
|91|Decode Ways||||
|92|Reverse Linked List II||||
|93|Restore IP Addresses||||
|94|Binary Tree Inorder Traversal||||
|95|Unique Binary Search Trees II||||
|96|Unique Binary Search Trees||||
|97|Interleaving String||||
|98|Validate Binary Search Tree||||
|99|Recover Binary Search Tree||||
|100|Same Tree||||
|101|Symmetric Tree||||
|102|Binary Tree Level Order Traversal||||
|103|Binary Tree Zigzag Level Order Traversal||||
|104|Maximum Depth of Binary Tree||||
|105|Construct Binary Tree from Preorder and Inorder Traversal||||
|106|Construct Binary Tree from Inorder and Postorder Traversal||||
|107|Binary Tree Level Order Traversal II||||
|108|Convert Sorted Array to Binary Search Tree||||
|109|Convert Sorted List to Binary Search Tree||||
|110|Balanced Binary Tree||||
|111|Minimum Depth of Binary Tree||||
|112|Path Sum||||
|113|Path Sum II||||
|114|Flatten Binary Tree to Linked List||||
|115|Distinct Subsequences||||
|116|Populating Next Right Pointers in Each Node||||
|117|Populating Next Right Pointers in Each Node II||||
|118|Pascal's Triangle||||
|119|Pascal's Triangle II||||
|120|Triangle||||
|121|Best Time to Buy and Sell Stock||||
|122|Best Time to Buy and Sell Stock II||||
|123|Best Time to Buy and Sell Stock III||||
|124|Binary Tree Maximum Path Sum||||
|125|Valid Palindrome||||
|126|Word Ladder II||||
|127|Word Ladder||||
|128|Longest Consecutive Sequence||||
|129|Sum Root to Leaf Numbers||||
|130|Surrounded Regions||||
|131|Palindrome Partitioning||||
|132|Palindrome Partitioning II||||
|133|Clone Graph||||
|134|Gas Station||||
|135|Candy||||
|136|Single Number||||
|137|Single Number II||||
|138|Copy List with Random Pointer||||
|139|Word Break||||
|140|Word Break II||||
|141|Linked List Cycle||||
|142|Linked List Cycle II||||
|143|Reorder List||||
|144|Binary Tree Preorder Traversal||||
|145|Binary Tree Postorder Traversal||||
|146|LRU Cache||||
|147|Insertion Sort List||||
|148|Sort List||||
|149|Max Points on a Line||||
|150|Evaluate Reverse Polish Notation||||
|151|Reverse Words in a String||||
|152|Maximum Product Subarray||||
|153|Find Minimum in Rotated Sorted Array||||
|154|Find Minimum in Rotated Sorted Array II||||
|155|Min Stack||||
|156|Binary Tree Upside Down||||
|157|Read N Characters Given Read4||||
|158|Read N Characters Given Read4 II - Call multiple times||||
|159|Longest Substring with At Most Two Distinct Characters||||
|160|Intersection of Two Linked Lists||||
|161|One Edit Distance||||
|162|Find Peak Element||||
|163|Missing Ranges||||
|164|Maximum Gap||||
|165|Compare Version Numbers||||
|166|Fraction to Recurring Decimal||||
|167|Two Sum II - Input array is sorted||||
|168|Excel Sheet Column Title||||
|169|Majority Element||||
|170|Two Sum III - Data structure design||||
|171|Excel Sheet Column Number||||
|172|Factorial Trailing Zeroes||||
|173|Binary Search Tree Iterator||||
|174|Dungeon Game||||
|175|Combine Two Tables||||
|176|Second Highest Salary||||
|177|Nth Highest Salary||||
|178|Rank Scores||||
|179|Largest Number||||
|180|Consecutive Numbers||||
|181|Employees Earning More Than Their Managers||||
|182|Duplicate Emails||||
|183|Customers Who Never Order||||
|184|Department Highest Salary||||
|185|Department Top Three Salaries||||
|186|Reverse Words in a String II||||
|187|Repeated DNA Sequences||||
|188|Best Time to Buy and Sell Stock IV||||
|189|Rotate Array||||
|190|Reverse Bits||||
|191|Number of 1 Bits||||
|192|Word Frequency||||
|193|Valid Phone Numbers||||
|194|Transpose File||||
|195|Tenth Line||||
|196|Delete Duplicate Emails||||
|197|Rising Temperature||||
|198|House Robber||||
|199|Binary Tree Right Side View||||
|200|Number of Islands||||
|201|Bitwise AND of Numbers Range||||
|202|Happy Number||||
|203|Remove Linked List Elements||||
|204|Count Primes||||
|205|Isomorphic Strings||||
|206|Reverse Linked List||||
|207|Course Schedule||||
|208|Implement Trie (Prefix Tree)||||
|209|Minimum Size Subarray Sum||||
|210|Course Schedule II||||
|211|Add and Search Word - Data structure design||||
|212|Word Search II||||
|213|House Robber II||||
|214|Shortest Palindrome||||
|215|Kth Largest Element in an Array||||
|216|Combination Sum III||||
|217|Contains Duplicate||||
|218|The Skyline Problem||||
|219|Contains Duplicate II||||
|220|Contains Duplicate III||||
|221|Maximal Square||||
|222|Count Complete Tree Nodes||||
|223|Rectangle Area||||
|224|Basic Calculator||||
|225|Implement Stack using Queues||||
|226|Invert Binary Tree||||
|227|Basic Calculator II||||
|228|Summary Ranges||||
|229|Majority Element II||||
|230|Kth Smallest Element in a BST||||
|231|Power of Two||||
|232|Implement Queue using Stacks||||
|233|Number of Digit One||||
|234|Palindrome Linked List||||
|235|Lowest Common Ancestor of a Binary Search Tree||||
|236|Lowest Common Ancestor of a Binary Tree||||
|237|Delete Node in a Linked List||||
|238|Product of Array Except Self||||
|239|Sliding Window Maximum||||
|240|Search a 2D Matrix II||||
|241|Different Ways to Add Parentheses||||
|242|Valid Anagram||||
|243|Shortest Word Distance||||
|244|Shortest Word Distance II||||
|245|Shortest Word Distance III||||
|246|Strobogrammatic Number||||
|247|Strobogrammatic Number II||||
|248|Strobogrammatic Number III||||
|249|Group Shifted Strings||||
|250|Count Univalue Subtrees||||
|251|Flatten 2D Vector||||
|252|Meeting Rooms||||
|253|Meeting Rooms II||||
|254|Factor Combinations||||
|255|Verify Preorder Sequence in Binary Search Tree||||
|256|Paint House||||
|257|Binary Tree Paths||||
|258|Add Digits||||
|259|3Sum Smaller||||
|260|Single Number III||||
|261|Graph Valid Tree||||
|262|Trips and Users||||
|263|Ugly Number||||
|264|Ugly Number II||||
|265|Paint House II||||
|266|Palindrome Permutation||||
|267|Palindrome Permutation II||||
|268|Missing Number||||
|269|Alien Dictionary||||
|270|Closest Binary Search Tree Value||||
|271|Encode and Decode Strings||||
|272|Closest Binary Search Tree Value II||||
|273|Integer to English Words||||
|274|H-Index||||
|275|H-Index II||||
|276|Paint Fence||||
|277|Find the Celebrity||||
|278|First Bad Version||||
|279|Perfect Squares||||
|280|Wiggle Sort||||
|281|Zigzag Iterator||||
|282|Expression Add Operators||||
|283|Move Zeroes||||
|284|Peeking Iterator||||
|285|Inorder Successor in BST||||
|286|Walls and Gates||||
|287|Find the Duplicate Number||||
|288|Unique Word Abbreviation||||
|289|Game of Life||||
|290|Word Pattern||||
|291|Word Pattern II||||
|292|Nim Game||||
|293|Flip Game||||
|294|Flip Game II||||
|295|Find Median from Data Stream||||
|296|Best Meeting Point||||
|297|Serialize and Deserialize Binary Tree||||
|298|Binary Tree Longest Consecutive Sequence||||
|299|Bulls and Cows||||
|300|Longest Increasing Subsequence||||
|301|Remove Invalid Parentheses||||
|302|Smallest Rectangle Enclosing Black Pixels||||
|303|Range Sum Query - Immutable||||
|304|Range Sum Query 2D - Immutable||||
|305|Number of Islands II||||
|306|Additive Number||||
|307|Range Sum Query - Mutable||||
|308|Range Sum Query 2D - Mutable||||
|309|Best Time to Buy and Sell Stock with Cooldown||||
|310|Minimum Height Trees||||
|311|Sparse Matrix Multiplication||||
|312|Burst Balloons||||
|313|Super Ugly Number||||
|314|Binary Tree Vertical Order Traversal||||
|315|Count of Smaller Numbers After Self||||
|316|Remove Duplicate Letters||||
|317|Shortest Distance from All Buildings||||
|318|Maximum Product of Word Lengths||||
|319|Bulb Switcher||||
|320|Generalized Abbreviation||||
|321|Create Maximum Number||||
|322|Coin Change||||
|323|Number of Connected Components in an Undirected Graph||||
|324|Wiggle Sort II||||
|325|Maximum Size Subarray Sum Equals k||||
|326|Power of Three||||
|327|Count of Range Sum||||
|328|Odd Even Linked List||||
|329|Longest Increasing Path in a Matrix||||
|330|Patching Array||||
|331|Verify Preorder Serialization of a Binary Tree||||
|332|Reconstruct Itinerary||||
|333|Largest BST Subtree||||
|334|Increasing Triplet Subsequence||||
|335|Self Crossing||||
|336|Palindrome Pairs||||
|337|House Robber III||||
|338|Counting Bits||||
|339|Nested List Weight Sum||||
|340|Longest Substring with At Most K Distinct Characters||||
|341|Flatten Nested List Iterator||||
|342|Power of Four||||
|343|Integer Break||||
|344|Reverse String||||
|345|Reverse Vowels of a String||||
|346|Moving Average from Data Stream||||
|347|Top K Frequent Elements||||
|348|Design Tic-Tac-Toe||||
|349|Intersection of Two Arrays||||
|350|Intersection of Two Arrays II||||
|351|Android Unlock Patterns||||
|352|Data Stream as Disjoint Intervals||||
|353|Design Snake Game||||
|354|Russian Doll Envelopes||||
|355|Design Twitter||||
|356|Line Reflection||||
|357|Count Numbers with Unique Digits||||
|358|Rearrange String k Distance Apart||||
|359|Logger Rate Limiter||||
|360|Sort Transformed Array||||
|361|Bomb Enemy||||
|362|Design Hit Counter||||
|363|Max Sum of Rectangle No Larger Than K||||
|364|Nested List Weight Sum II||||
|365|Water and Jug Problem||||
|366|Find Leaves of Binary Tree||||
|367|Valid Perfect Square||||
|368|Largest Divisible Subset||||
|369|Plus One Linked List||||
|370|Range Addition||||
|371|Sum of Two Integers||||
|372|Super Pow||||
|373|Find K Pairs with Smallest Sums||||
|374|Guess Number Higher or Lower||||
|375|Guess Number Higher or Lower II||||
|376|Wiggle Subsequence||||
|377|Combination Sum IV||||
|378|Kth Smallest Element in a Sorted Matrix||||
|379|Design Phone Directory||||
|380|Insert Delete GetRandom O(1)||||
|381|Insert Delete GetRandom O(1) - Duplicates allowed||||
|382|Linked List Random Node||||
|383|Ransom Note||||
|384|Shuffle an Array||||
|385|Mini Parser||||
|386|Lexicographical Numbers||||
|387|First Unique Character in a String||||
|388|Longest Absolute File Path||||
|389|Find the Difference||||
|390|Elimination Game||||
|391|Perfect Rectangle||||
|392|Is Subsequence||||
|393|UTF-8 Validation||||
|394|Decode String||||
|395|Longest Substring with At Least K Repeating Characters||||
|396|Rotate Function||||
|397|Integer Replacement||||
|398|Random Pick Index||||
|399|Evaluate Division||||
|400|Nth Digit||||
|401|Binary Watch||||
|402|Remove K Digits||||
|403|Frog Jump||||
|404|Sum of Left Leaves||||
|405|Convert a Number to Hexadecimal||||
|406|Queue Reconstruction by Height||||
|407|Trapping Rain Water II||||
|408|Valid Word Abbreviation||||
|409|Longest Palindrome||||
|410|Split Array Largest Sum||||
|411|Minimum Unique Word Abbreviation||||
|412|Fizz Buzz||||
|413|Arithmetic Slices||||
|414|Third Maximum Number||||
|415|Add Strings||||
|416|Partition Equal Subset Sum||||
|417|Pacific Atlantic Water Flow||||
|418|Sentence Screen Fitting||||
|419|Battleships in a Board||||
|420|Strong Password Checker||||
|421|Maximum XOR of Two Numbers in an Array||||
|422|Valid Word Square||||
|423|Reconstruct Original Digits from English||||
|424|Longest Repeating Character Replacement||||
|425|Word Squares||||
|426|Convert Binary Search Tree to Sorted Doubly Linked List||||
|427|Construct Quad Tree||||
|428|Serialize and Deserialize N-ary Tree||||
|429|N-ary Tree Level Order Traversal||||
|430|Flatten a Multilevel Doubly Linked List||||
|431|Encode N-ary Tree to Binary Tree||||
|432|All O`one Data Structure||||
|433|Minimum Genetic Mutation||||
|434|Number of Segments in a String||||
|435|Non-overlapping Intervals||||
|436|Find Right Interval||||
|437|Path Sum III||||
|438|Find All Anagrams in a String||||
|439|Ternary Expression Parser||||
|440|K-th Smallest in Lexicographical Order||||
|441|Arranging Coins||||
|442|Find All Duplicates in an Array||||
|443|String Compression||||
|444|Sequence Reconstruction||||
|445|Add Two Numbers II||||
|446|Arithmetic Slices II - Subsequence||||
|447|Number of Boomerangs||||
|448|Find All Numbers Disappeared in an Array||||
|449|Serialize and Deserialize BST||||
|450|Delete Node in a BST||||
|451|Sort Characters By Frequency||||
|452|Minimum Number of Arrows to Burst Balloons||||
|453|Minimum Moves to Equal Array Elements||||
|454|4Sum II||||
|455|Assign Cookies||||
|456|132 Pattern||||
|457|Circular Array Loop||||
|458|Poor Pigs||||
|459|Repeated Substring Pattern||||
|460|LFU Cache||||
|461|Hamming Distance||||
|462|Minimum Moves to Equal Array Elements II||||
|463|Island Perimeter||||
|464|Can I Win||||
|465|Optimal Account Balancing||||
|466|Count The Repetitions||||
|467|Unique Substrings in Wraparound String||||
|468|Validate IP Address||||
|469|Convex Polygon||||
|470|Implement Rand10() Using Rand7()||||
|471|Encode String with Shortest Length||||
|472|Concatenated Words||||
|473|Matchsticks to Square||||
|474|Ones and Zeroes||||
|475|Heaters||||
|476|Number Complement||||
|477|Total Hamming Distance||||
|478|Generate Random Point in a Circle||||
|479|Largest Palindrome Product||||
|480|Sliding Window Median||||
|481|Magical String||||
|482|License Key Formatting||||
|483|Smallest Good Base||||
|484|Find Permutation||||
|485|Max Consecutive Ones||||
|486|Predict the Winner||||
|487|Max Consecutive Ones II||||
|488|Zuma Game||||
|489|Robot Room Cleaner||||
|490|The Maze||||
|491|Increasing Subsequences||||
|492|Construct the Rectangle||||
|493|Reverse Pairs||||
|494|Target Sum||||
|495|Teemo Attacking||||
|496|Next Greater Element I||||
|497|Random Point in Non-overlapping Rectangles||||
|498|Diagonal Traverse||||
|499|The Maze III||||
|500|Keyboard Row||||
|501|Find Mode in Binary Search Tree||||
|502|IPO||||
|503|Next Greater Element II||||
|504|Base 7||||
|505|The Maze II||||
|506|Relative Ranks||||
|507|Perfect Number||||
|508|Most Frequent Subtree Sum||||
|509|Find Bottom Left Tree Value||||
|510|Freedom Trail||||
|511|Find Largest Value in Each Tree Row||||
|512|Longest Palindromic Subsequence||||
|513|Super Washing Machines||||
|514|Coin Change 2||||
|515|Random Flip Matrix||||
|516|Detect Capital||||
|517|Longest Uncommon Subsequence I ||||
|518|Longest Uncommon Subsequence II||||
|519|Continuous Subarray Sum||||
|520|Longest Word in Dictionary through Deleting||||
|521|Contiguous Array||||
|522|Beautiful Arrangement||||
|523|Word Abbreviation||||
|524|Random Pick with Weight||||
|525|Minesweeper||||
|526|Minimum Absolute Difference in BST||||
|527|Lonely Pixel I||||
|528|K-diff Pairs in an Array||||
|529|Lonely Pixel II||||
|530|Encode and Decode TinyURL||||
|531|Construct Binary Tree from String||||
|532|Complex Number Multiplication||||
|533|Convert BST to Greater Tree||||
|534|Minimum Time Difference||||
|535|Single Element in a Sorted Array||||
|536|Reverse String II||||
|537|01 Matrix||||
|538|Diameter of Binary Tree||||
|539|Output Contest Matches||||
|540|Boundary of Binary Tree||||
|541|Remove Boxes||||
|542|Friend Circles||||
|543|Split Array with Equal Sum||||
|544|Binary Tree Longest Consecutive Sequence II||||
|545|Student Attendance Record I||||
|546|Student Attendance Record II||||
|547|Optimal Division||||
|548|Brick Wall||||
|549|Split Concatenated Strings||||
|550|Next Greater Element III||||
|551|Reverse Words in a String III||||
|552|Quad Tree Intersection||||
|553|Maximum Depth of N-ary Tree||||
|554|Subarray Sum Equals K||||
|555|Array Partition I||||
|556|Longest Line of Consecutive One in Matrix||||
|557|Binary Tree Tilt||||
|558|Find the Closest Palindrome||||
|559|Array Nesting||||
|560|Reshape the Matrix||||
|561|Permutation in String||||
|562|Maximum Vacation Days||||
|563|Median Employee Salary||||
|564|Managers with at Least 5 Direct Reports||||
|565|Find Median Given Frequency of Numbers||||
|566|Subtree of Another Tree||||
|567|Squirrel Simulation||||
|568|Winning Candidate||||
|569|Distribute Candies||||
|570|Out of Boundary Paths||||
|571|Employee Bonus||||
|572|Get Highest Answer Rate Question||||
|573|Find Cumulative Salary of an Employee||||
|574|Count Student Number in Departments||||
|575|Shortest Unsorted Continuous Subarray||||
|576|Kill Process||||
|577|Delete Operation for Two Strings||||
|578|Find Customer Referee||||
|579|Investments in 2016||||
|580|Customer Placing the Largest Number of Orders||||
|581|Erect the Fence||||
|582|Design In-Memory File System||||
|583|N-ary Tree Preorder Traversal||||
|584|N-ary Tree Postorder Traversal||||
|585|Tag Validator||||
|586|Fraction Addition and Subtraction||||
|587|Valid Square||||
|588|Longest Harmonious Subsequence||||
|589|Big Countries||||
|590|Classes More Than 5 Students||||
|591|Friend Requests I: Overall Acceptance Rate||||
|592|Range Addition II||||
|593|Minimum Index Sum of Two Lists||||
|594|Non-negative Integers without Consecutive Ones||||
|595|Human Traffic of Stadium||||
|596|Friend Requests II: Who Has the Most Friends||||
|597|Consecutive Available Seats||||
|598|Design Compressed String Iterator||||
|599|Can Place Flowers||||
|600|Construct String from Binary Tree||||
|601|Sales Person||||
|602|Tree Node||||
|603|Find Duplicate File in System||||
|604|Triangle Judgement||||
|605|Valid Triangle Number||||
|606|Shortest Distance in a Plane||||
|607|Shortest Distance in a Line||||
|608|Second Degree Follower||||
|609|Average Salary: Departments VS Company||||
|610|Add Bold Tag in String||||
|611|Merge Two Binary Trees||||
|612|Students Report By Geography||||
|613|Biggest Single Number||||
|614|Not Boring Movies||||
|615|Task Scheduler||||
|616|Design Circular Queue||||
|617|Add One Row to Tree||||
|618|Maximum Distance in Arrays||||
|619|Minimum Factorization||||
|620|Exchange Seats||||
|621|Swap Salary||||
|622|Maximum Product of Three Numbers||||
|623|K Inverse Pairs Array||||
|624|Course Schedule III||||
|625|Design Excel Sum Formula||||
|626|Smallest Range||||
|627|Sum of Square Numbers||||
|628|Find the Derangement of An Array||||
|629|Design Log Storage System||||
|630|Exclusive Time of Functions||||
|631|Average of Levels in Binary Tree||||
|632|Shopping Offers||||
|633|Decode Ways II||||
|634|Solve the Equation||||
|635|Design Circular Deque||||
|636|Design Search Autocomplete System||||
|637|Maximum Average Subarray I||||
|638|Maximum Average Subarray II||||
|639|Set Mismatch||||
|640|Maximum Length of Pair Chain||||
|641|Palindromic Substrings||||
|642|Replace Words||||
|643|Dota2 Senate||||
|644|2 Keys Keyboard||||
|645|4 Keys Keyboard||||
|646|Find Duplicate Subtrees||||
|647|Two Sum IV - Input is a BST||||
|648|Maximum Binary Tree||||
|649|Print Binary Tree||||
|650|Coin Path||||
|651|Judge Route Circle||||
|652|Find K Closest Elements||||
|653|Split Array into Consecutive Subsequences||||
|654|Remove 9||||
|655|Image Smoother||||
|656|Maximum Width of Binary Tree||||
|657|Equal Tree Partition||||
|658|Strange Printer||||
|659|Non-decreasing Array||||
|660|Path Sum IV||||
|661|Beautiful Arrangement II||||
|662|Kth Smallest Number in Multiplication Table||||
|663|Trim a Binary Search Tree||||
|664|Maximum Swap||||
|665|Second Minimum Node In a Binary Tree||||
|666|Bulb Switcher II||||
|667|Number of Longest Increasing Subsequence||||
|668|Longest Continuous Increasing Subsequence||||
|669|Cut Off Trees for Golf Event||||
|670|Implement Magic Dictionary||||
|671|Map Sum Pairs||||
|672|Valid Parenthesis String||||
|673|24 Game||||
|674|Valid Palindrome II||||
|675|Next Closest Time||||
|676|Baseball Game||||
|677|K Empty Slots||||
|678|Redundant Connection||||
|679|Redundant Connection II||||
|680|Repeated String Match||||
|681|Longest Univalue Path||||
|682|Knight Probability in Chessboard||||
|683|Maximum Sum of 3 Non-Overlapping Subarrays||||
|684|Employee Importance||||
|685|Stickers to Spell Word||||
|686|Top K Frequent Words||||
|687|Binary Number with Alternating Bits||||
|688|Number of Distinct Islands||||
|689|Max Area of Island||||
|690|Count Binary Substrings||||
|691|Degree of an Array||||
|692|Partition to K Equal Sum Subsets||||
|693|Falling Squares||||
|694|Search in a Binary Search Tree||||
|695|Insert into a Binary Search Tree||||
|696|Search in a Sorted Array of Unknown Size||||
|697|Kth Largest Element in a Stream||||
|698|Binary Search||||
|699|Design HashSet||||
|700|Design HashMap||||
|701|Design Linked List||||
|702|Insert into a Cyclic Sorted List||||
|703|To Lower Case||||
|704|Random Pick with Blacklist||||
|705|Number of Distinct Islands II||||
|706|Minimum ASCII Delete Sum for Two Strings||||
|707|Subarray Product Less Than K||||
|708|Best Time to Buy and Sell Stock with Transaction Fee||||
|709|Range Module||||
|710|Max Stack||||
|711|1-bit and 2-bit Characters||||
|712|Maximum Length of Repeated Subarray||||
|713|Find K-th Smallest Pair Distance||||
|714|Longest Word in Dictionary||||
|715|Accounts Merge||||
|716|Remove Comments||||
|717|Candy Crush||||
|718|Find Pivot Index||||
|719|Split Linked List in Parts||||
|720|Number of Atoms||||
|721|Minimum Window Subsequence||||
|722|Self Dividing Numbers||||
|723|My Calendar I||||
|724|Count Different Palindromic Subsequences||||
|725|My Calendar II||||
|726|My Calendar III||||
|727|Flood Fill||||
|728|Sentence Similarity||||
|729|Asteroid Collision||||
|730|Parse Lisp Expression||||
|731|Sentence Similarity II||||
|732|Monotone Increasing Digits||||
|733|Daily Temperatures||||
|734|Delete and Earn||||
|735|Cherry Pickup||||
|736|Closest Leaf in a Binary Tree||||
|737|Network Delay Time||||
|738|Find Smallest Letter Greater Than Target||||
|739|Prefix and Suffix Search||||
|740|Min Cost Climbing Stairs||||
|741|Largest Number At Least Twice of Others||||
|742|Shortest Completing Word||||
|743|Contain Virus||||
|744|Number Of Corner Rectangles||||
|745|IP to CIDR||||
|746|Open the Lock||||
|747|Cracking the Safe||||
|748|Reach a Number||||
|749|Pour Water||||
|750|Pyramid Transition Matrix||||
|751|Set Intersection Size At Least Two||||
|752|Bold Words in String||||
|753|Employee Free Time||||
|754|Find Anagram Mappings||||
|755|Special Binary String||||
|756|Prime Number of Set Bits in Binary Representation||||
|757|Partition Labels||||
|758|Largest Plus Sign||||
|759|Couples Holding Hands||||
|760|Toeplitz Matrix||||
|761|Reorganize String||||
|762|Max Chunks To Make Sorted II||||
|763|Max Chunks To Make Sorted||||
|764|Basic Calculator IV||||
|765|Jewels and Stones||||
|766|Basic Calculator III||||
|767|Sliding Puzzle||||
|768|Minimize Max Distance to Gas Station||||
|769|Global and Local Inversions||||
|770|Split BST||||
|771|Swap Adjacent in LR String||||
|772|Swim in Rising Water||||
|773|K-th Symbol in Grammar||||
|774|Reaching Points||||
|775|Rabbits in Forest||||
|776|Transform to Chessboard||||
|777|Minimum Distance Between BST Nodes||||
|778|Letter Case Permutation||||
|779|Is Graph Bipartite?||||
|780|K-th Smallest Prime Fraction||||
|781|Cheapest Flights Within K Stops||||
|782|Rotated Digits||||
|783|Escape The Ghosts||||
|784|Domino and Tromino Tiling||||
|785|Custom Sort String||||
|786|Number of Matching Subsequences||||
|787|Preimage Size of Factorial Zeroes Function||||
|788|Valid Tic-Tac-Toe State||||
|789|Number of Subarrays with Bounded Maximum||||
|790|Rotate String||||
|791|All Paths From Source to Target||||
|792|Smallest Rotation with Highest Score||||
|793|Champagne Tower||||
|794|Similar RGB Color||||
|795|Minimum Swaps To Make Sequences Increasing||||
|796|Find Eventual Safe States||||
|797|Bricks Falling When Hit||||
|798|Unique Morse Code Words||||
|799|Split Array With Same Average||||
|800|Number of Lines To Write String||||
|801|Max Increase to Keep City Skyline||||
|802|Soup Servings||||
|803|Expressive Words||||
|804|Chalkboard XOR Game||||
|805|Subdomain Visit Count||||
|806|Largest Triangle Area||||
|807|Largest Sum of Averages||||
|808|Binary Tree Pruning||||
|809|Bus Routes||||
|810|Ambiguous Coordinates||||
|811|Linked List Components||||
|812|Race Car||||
|813|Most Common Word||||
|814|Short Encoding of Words||||
|815|Shortest Distance to a Character||||
|816|Card Flipping Game||||
|817|Binary Trees With Factors||||
|818|Goat Latin||||
|819|Friends Of Appropriate Ages||||
|820|Most Profit Assigning Work||||
|821|Making A Large Island||||
|822|Unique Letter String||||
|823|Consecutive Numbers Sum||||
|824|Positions of Large Groups||||
|825|Masking Personal Information||||
|826|Flipping an Image||||
|827|Find And Replace in String||||
|828|Sum of Distances in Tree||||
|829|Image Overlap||||
|830|Rectangle Overlap||||
|831|New 21 Game||||
|832|Push Dominoes||||
|833|Similar String Groups||||
|834|Magic Squares In Grid||||
|835|Keys and Rooms||||
|836|Split Array into Fibonacci Sequence||||
|837|Guess the Word||||
|838|Backspace String Compare||||
|839|Longest Mountain in Array||||
|840|Hand of Straights||||
|841|Shortest Path Visiting All Nodes||||
|842|Shifting Letters||||
|843|Maximize Distance to Closest Person||||
|844|Rectangle Area II||||
|845|Loud and Rich||||
|846|Peak Index in a Mountain Array||||
|847|Car Fleet||||
|848|K-Similar Strings||||
|849|Exam Room||||
|850|Score of Parentheses||||
|851|Minimum Cost to Hire K Workers||||
|852|Mirror Reflection||||
|853|Buddy Strings||||
|854|Lemonade Change||||
|855|Score After Flipping Matrix||||
|856|Shortest Subarray with Sum at Least K||||
|857|All Nodes Distance K in Binary Tree||||
|858|Shortest Path to Get All Keys||||
|859|Smallest Subtree with all the Deepest Nodes||||
|860|Prime Palindrome||||
|861|Transpose Matrix||||
|862|Binary Gap||||
|863|Reordered Power of 2||||
|864|Advantage Shuffle||||
|865|Minimum Number of Refueling Stops||||
|866|Leaf-Similar Trees||||
|867|Length of Longest Fibonacci Subsequence||||
|868|Walking Robot Simulation||||
|869|Koko Eating Bananas||||
|870|Middle of the Linked List||||
|871|Stone Game||||
|872|Nth Magical Number||||
|873|Profitable Schemes||||
|874|Decoded String at Index||||
|875|Boats to Save People||||
|876|Reachable Nodes In Subdivided Graph||||
|877|Projection Area of 3D Shapes||||
|878|Uncommon Words from Two Sentences||||
|879|Spiral Matrix III||||
|880|Possible Bipartition||||
|881|Super Egg Drop||||
